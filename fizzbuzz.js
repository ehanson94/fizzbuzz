function fizzbuzz(maxValue) {
    let myString = "";
    for(let i=1; i<=maxValue; i++) {
        if (i%2==0 && i%3==0) {
            myString = myString.concat("Fizzbuzz" + ",")
        }
        else if (i%2==0) {
            myString = myString.concat("Fizz" + ",")
        }
        else if (i%3==0) {
            myString = myString.concat("Buzz" + ",")
        }
        else {
            myString = myString.concat(i + ",")
        }
        }
        return myString;
    }
console.log(fizzbuzz(1000));